import { createApp } from 'vue';
import App from './App.vue';
// 路由
import router from '@/router/index.ts';
// 状态管理
import { createPinia } from 'pinia';

// 样式重置
import 'normalize.css/normalize.css';

const app = createApp(App);

const store = createPinia();

// 注册路由
app.use(router);

// 注册状态管理
app.use(store);

app.mount('#app');
