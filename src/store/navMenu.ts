import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useNavMenuStore = defineStore('navMenu', () => {
  const navMenuState = ref(false);

  return {
    navMenuState,
    toggleNavMenu() {
      navMenuState.value = !navMenuState.value;
    },
  };
});
