import { createRouter, createWebHistory } from 'vue-router';

import Layout from '@/layout/index.vue';

const routes = [
  {
    path: '/login',
    name: 'Login',
    meta: { title: '登录' },
    component: () => import('@/views/Login/index.vue'),
  },
  {
    path: '/',
    component: Layout,
    redirect: 'home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/Home/index.vue'),
        name: 'Home',
        meta: { title: '首页' },
      },
    ],
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
  {
    path: '/404',
    name: 'NotFound',
    meta: { title: '404' },
    component: () => import('@/views/NotFound/index.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// 设置页面标题
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = <string>to.meta.title;
  }
  next();
});

export default router;
