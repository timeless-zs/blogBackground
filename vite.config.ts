import { defineConfig } from 'vite';
import { resolve } from 'path';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers';

export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [
        AntDesignVueResolver({
          importStyle: false, // css in js
        }),
      ],
    }),
  ],
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': resolve(__dirname, 'src'),
      views: resolve(__dirname, 'src/views'),
      components: resolve(__dirname, 'src/components'),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        math: 'always',
        modifyVars: {
          hack: `true; @import (reference) "${resolve(
            'src/assets/css/variable.less'
          )}";`,
        },
        javascriptEnabled: true,
      },
    },
  },
});
